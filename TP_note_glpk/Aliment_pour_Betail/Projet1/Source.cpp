#include <stdio.h>
#include <stdlib.h>
#include <sstream> 
#include <iostream>


using namespace std;

#include "glpk.h"



void main(void)
{
	// les constantes
	const int mp = 3;
	const int b = 3;
	const int n = 2;
	const int nb_op = 4;

	// les tableaux
	string MatPremNom[mp + 1];
	int Qmax[mp + 1];
	double Pr[mp + 1];
	string OperationNom[nb_op + 1];
	double C[nb_op+1];
	string NutrimentNom[b+1];
	double BN[b + 1];
	double P[mp + 1][b + 1];
	double D[n + 1];


	// nom des mat premieres
	MatPremNom[1] = "Avoine";
	MatPremNom[2] = "Mais";
	MatPremNom[3] = "Melasse";
	// qtes des mat premieres
	Qmax[1] = 11900;
	Qmax[2] = 23500;
	Qmax[3] = 750;
	// prix des mat premieres
	Pr[1] = 0.8;
	Pr[2] = 1.0;
	Pr[3] = 0.75;
	// nom des operations
	OperationNom[1] = "Mixage";
	OperationNom[2] = "Melange";
	OperationNom[3] = "Granulation";
	OperationNom[4] = "Tamisage";

	// cout des operations
	C[1] = 1.5;
	C[2] = 0.3;
	C[3] = 2.5;
	C[4] = 1;

	// noms des nutriments
	NutrimentNom[1] = "Proteines";
	NutrimentNom[2] = "Lipides";
	NutrimentNom[3] = "Fibres";

	// teneurs recommand�es
	BN[1] = 0.095;
	BN[2] = 0.02;
	BN[3] = 0.06;

	// composition des mati�res premi�res dans les diff�rents nutriments
	P[1][1] = 0.136;
	P[1][2] = 0.071;
	P[1][3] = 0.07;

	P[2][1] = 0.041;
	P[2][2] = 0.024;
	P[2][3] = 0.037;

	P[3][1] = 0.05;
	P[3][2] = 0.003;
	P[3][3] = 0.25;

	// Quantit�s demand�es
	D[1] = 9000;
	D[2] = 12000;


	// creation des tableaux pour le solveur
	glp_prob *lp;
	int ia[1 + 1000], ja[1 + 1000];
	double ar[1 + 1000], z, x1, x2;

	// definition du probleme
	lp = glp_create_prob();
	glp_set_prob_name(lp, "Aliment");
	glp_set_obj_dir(lp, GLP_MIN);


	// variable globales pour la generation
	int CompteurContrainte = 0;
	int CompteurCol = 0;
	int CompteurIA = 0;
	


	string nomcol;
	string nomcontrainte;
	int position;

	// definition des variables - Slides 45
	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= mp; j++)
		{
			std::ostringstream oss;
			oss << "x_" << i << '_' << j;
			nomcol = oss.str();
			glp_add_cols(lp,1);
			CompteurCol = CompteurCol + 1;
			glp_set_col_name(lp, CompteurCol, nomcol.c_str());
			glp_set_col_kind(lp, CompteurCol, GLP_IV);
			glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);
		}
	}
	for (int i = 1; i <= n; i++)
	{
		std::ostringstream oss;
		oss << "q_" << i;
		nomcol = oss.str();
		glp_add_cols(lp, 1);
		CompteurCol = CompteurCol + 1;
		glp_set_col_name(lp, CompteurCol, nomcol.c_str());
		// glp_set_col_kind(lp, CompteurCol, GLP_IV);
		glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);
	}

	// declaration des variables z de la fonctions objective
	std::ostringstream oss;
	oss << "z1";
	nomcol = oss.str();
	glp_add_cols(lp, 1);
	CompteurCol = CompteurCol + 1;
	glp_set_col_name(lp, CompteurCol, nomcol.c_str());
	glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);

	oss.str("");
	oss << "z2";
	nomcol = oss.str();
	glp_add_cols(lp, 1);
	CompteurCol = CompteurCol + 1;
	glp_set_col_name(lp, CompteurCol, nomcol.c_str());
	glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);

	oss.str("");
	oss << "z3";
	nomcol = oss.str();
	glp_add_cols(lp, 1);
	CompteurCol = CompteurCol + 1;
	glp_set_col_name(lp, CompteurCol, nomcol.c_str());
	glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);

	oss.str("");
	oss << "z4";
	nomcol = oss.str();
	glp_add_cols(lp, 1);
	CompteurCol = CompteurCol + 1;
	glp_set_col_name(lp, CompteurCol, nomcol.c_str());
	glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);

	oss.str("");
	oss << "z5";
	nomcol = oss.str();
	glp_add_cols(lp, 1);
	CompteurCol = CompteurCol + 1;
	glp_set_col_name(lp, CompteurCol, nomcol.c_str());
	glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);




	// creation Index pour les recherches
	glp_create_index(lp);

	// contrainte 1.
	// -----------
	for (int i = 1; i <= n; i++)
	{
		glp_add_rows(lp, 1);
		CompteurContrainte++;

		std::ostringstream oss;
		oss << "C_" << CompteurContrainte;
		nomcontrainte = oss.str();

		glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
		glp_set_row_bnds(lp, CompteurContrainte, GLP_FX, 0.0, 0.0);

		//std::ostringstream oss;
		oss.str("");
		oss << "q_" << i;
		nomcol = oss.str();

		position = glp_find_col(lp, nomcol.c_str());
		CompteurIA = CompteurIA + 1;
		ia[CompteurIA] = CompteurContrainte; 
		ja[CompteurIA] = position; 
		ar[CompteurIA] = -1.0; // coef x


		for (int j = 1; j <= mp; j++)
		{
			// parcours des colonnes
			// x11 + x12 + x13 > D[1]

			oss.str("");
			oss << "x_" << i << "_" << j;
			nomcol = oss.str();

			position = glp_find_col(lp, nomcol.c_str());
			CompteurIA = CompteurIA + 1;
			ia[CompteurIA] = CompteurContrainte;
			ja[CompteurIA] = position;
			ar[CompteurIA] = 1.0; // coef x
		}
	}
	for (int i = 1; i <= n; i++)
	{
		glp_add_rows(lp, 1);
		CompteurContrainte++;

		std::ostringstream oss;
		oss << "C_" << CompteurContrainte;
		nomcontrainte = oss.str();

		glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
		glp_set_row_bnds(lp, CompteurContrainte, GLP_LO, D[i], 0.0);

		//std::ostringstream oss;
		oss.str("");
		oss << "q_" << i;
		nomcol = oss.str();

		position = glp_find_col(lp, nomcol.c_str());
		CompteurIA = CompteurIA + 1;
		ia[CompteurIA] = CompteurContrainte;
		ja[CompteurIA] = position;
		ar[CompteurIA] = 1.0; // coef x


	}

	int j = 1;
	// contrainte 2
	// ------------
	for (int i = 1; i <= n; i++)
	{
		for (int k = 1; k <= b-1; k++)
		{
			glp_add_rows(lp, 1);
			CompteurContrainte++;

			std::ostringstream oss;
			oss << "C_" << CompteurContrainte;
			nomcontrainte = oss.str();

			glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
			glp_set_row_bnds(lp, CompteurContrainte, GLP_LO, 0.0, 0.0);

			//std::ostringstream oss;
			oss.str("");
			oss << "q_" << i;
			nomcol = oss.str();

			position = glp_find_col(lp, nomcol.c_str());
			CompteurIA = CompteurIA + 1;
			ia[CompteurIA] = CompteurContrainte;
			ja[CompteurIA] = position;
			ar[CompteurIA] = - BN[k]; // coef x

			for (int j = 1; j <= b; j++)
			{

				oss.str("");
				oss << "x_" << i << "_" << j;
				nomcol = oss.str();

				position = glp_find_col(lp, nomcol.c_str());
				CompteurIA = CompteurIA + 1;
				ia[CompteurIA] = CompteurContrainte;
				ja[CompteurIA] = position;
				ar[CompteurIA] = P[j][k]; // coef x
			}
		}

	}



	// contrainte 3
	// ------------
	// k = 3
	for (int i = 1; i <= n; i++)
	{
		glp_add_rows(lp, 1);
		CompteurContrainte++;

		std::ostringstream oss;
		oss << "C_" << CompteurContrainte;
		nomcontrainte = oss.str();

		glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
		glp_set_row_bnds(lp, CompteurContrainte, GLP_UP, 0.0, 0.0);

		//std::ostringstream oss;
		oss.str("");
		oss << "q_" << i;
		nomcol = oss.str();

		position = glp_find_col(lp, nomcol.c_str());
		CompteurIA = CompteurIA + 1;
		ia[CompteurIA] = CompteurContrainte;
		ja[CompteurIA] = position;
		ar[CompteurIA] = -BN[3]; // coef x

		for (int j = 1; j <= b; j++)
		{
			oss.str("");
			oss << "x_" << i << "_" << j;
			nomcol = oss.str();

			position = glp_find_col(lp, nomcol.c_str());
			CompteurIA = CompteurIA + 1;
			ia[CompteurIA] = CompteurContrainte;
			ja[CompteurIA] = position;
			ar[CompteurIA] = P[j][3]; // coef x
		}
			
	}




	// containte 4
	// -----------
	
	for (int j = 1; j <= mp; j++)
	{

		glp_add_rows(lp, 1);
		CompteurContrainte++;

		std::ostringstream oss;
		oss << "C_" << CompteurContrainte;
		nomcontrainte = oss.str();

		glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
		glp_set_row_bnds(lp, CompteurContrainte, GLP_UP, 0.0, Qmax[j]);


		for (int i = 1; i <= n; i++)
		{
			//std::ostringstream oss;
			oss.str("");
			oss << "x_" << i << "_" << j;
			nomcol = oss.str();

			position = glp_find_col(lp, nomcol.c_str());
			CompteurIA = CompteurIA + 1;
			ia[CompteurIA] = CompteurContrainte;
			ja[CompteurIA] = position;
			ar[CompteurIA] = 1.0; // coef x
		}

	}

	

	// fonction objectif
	// -----------------

	// contrainte 5 pour z Z1
	for (int k = 1; k <= 1; k++)
	{
	
		glp_add_rows(lp, 1);
		CompteurContrainte++;

		std::ostringstream oss;
		oss << "C_" << CompteurContrainte;
		nomcontrainte = oss.str();

		glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
		glp_set_row_bnds(lp, CompteurContrainte, GLP_FX, 0.0, 0.0);

		oss.str("");
		oss << "z1";
		nomcol = oss.str();

		position = glp_find_col(lp, nomcol.c_str());
		CompteurIA = CompteurIA + 1;
		ia[CompteurIA] = CompteurContrainte;
		ja[CompteurIA] = position;
		ar[CompteurIA] = -1.0; 

		for (int i = 1; i <= n; i++)
		{
			for (int j = 1; j <= mp; j++)
			{

				oss.str("");
				oss << "x_" << i << "_" << j;
				nomcol = oss.str();

				position = glp_find_col(lp, nomcol.c_str());
				CompteurIA = CompteurIA + 1;
				ia[CompteurIA] = CompteurContrainte;
				ja[CompteurIA] = position;
				ar[CompteurIA] = Pr[j];
			}
		}

	}

	// contrainte 6 pour z Z2
	for (int k = 1; k <= 1; k++)
	{
	
		glp_add_rows(lp, 1);
		CompteurContrainte++;

		std::ostringstream oss;
		oss << "C_" << CompteurContrainte;
		nomcontrainte = oss.str();

		glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
		glp_set_row_bnds(lp, CompteurContrainte, GLP_FX, 0.0, 0.0);

		oss.str("");
		oss << "z2";
		nomcol = oss.str();

		position = glp_find_col(lp, nomcol.c_str());
		CompteurIA = CompteurIA + 1;
		ia[CompteurIA] = CompteurContrainte;
		ja[CompteurIA] = position;
		ar[CompteurIA] = -1.0;

		for (int i = 1; i <= n; i++)
		{
			for (int j = 1; j <= mp - 1; j++)
			{

				oss.str("");
				oss << "x_" << i << "_" << j;
				nomcol = oss.str();

				position = glp_find_col(lp, nomcol.c_str());
				CompteurIA = CompteurIA + 1;
				ia[CompteurIA] = CompteurContrainte;
				ja[CompteurIA] = position;
				ar[CompteurIA] = C[1];
			}
		}

	}

	// Z3

	for (int k = 1; k <= 1; k++)
	{

		glp_add_rows(lp, 1);
		CompteurContrainte++;

		std::ostringstream oss;
		oss << "C_" << CompteurContrainte;
		nomcontrainte = oss.str();

		glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
		glp_set_row_bnds(lp, CompteurContrainte, GLP_FX, 0.0, 0.0);

		oss.str("");
		oss << "z3";
		nomcol = oss.str();

		position = glp_find_col(lp, nomcol.c_str());
		CompteurIA = CompteurIA + 1;
		ia[CompteurIA] = CompteurContrainte;
		ja[CompteurIA] = position;
		ar[CompteurIA] = -1.0;

		for (int i = 1; i <= n; i++)
		{
			for (int j = 1; j <= mp; j++)
			{

				oss.str("");
				oss << "x_" << i << "_" << j;
				nomcol = oss.str();

				position = glp_find_col(lp, nomcol.c_str());
				CompteurIA = CompteurIA + 1;
				ia[CompteurIA] = CompteurContrainte;
				ja[CompteurIA] = position;
				ar[CompteurIA] = C[2];
			}
		}

	}

	// Z4


	for (int i = 1; i <= 1; i++)
	{

		glp_add_rows(lp, 1);
		CompteurContrainte++;

		oss.str("");
		oss << "C_" << CompteurContrainte;
		nomcontrainte = oss.str();

		glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
		glp_set_row_bnds(lp, CompteurContrainte, GLP_FX, 0.0, 0.0);

		oss.str("");
		oss << "z4";
		nomcol = oss.str();

		position = glp_find_col(lp, nomcol.c_str());
		CompteurIA = CompteurIA + 1;
		ia[CompteurIA] = CompteurContrainte;
		ja[CompteurIA] = position;
		ar[CompteurIA] = -1.0;

		for (int j = 1; j <= mp; j++)
		{

			oss.str("");
			oss << "x_" << 1 << "_" << j;
			nomcol = oss.str();

			position = glp_find_col(lp, nomcol.c_str());
			CompteurIA = CompteurIA + 1;
			ia[CompteurIA] = CompteurContrainte;
			ja[CompteurIA] = position;
			ar[CompteurIA] = C[3];
		}

	}

	// Z5

	for (int i = 1; i <= 1; i++)
	{

		glp_add_rows(lp, 1);
		CompteurContrainte++;

		std::ostringstream oss;
		oss << "C_" << CompteurContrainte;
		nomcontrainte = oss.str();

		glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
		glp_set_row_bnds(lp, CompteurContrainte, GLP_FX, 0.0, 0.0);

		oss.str("");
		oss << "z5";
		nomcol = oss.str();

		position = glp_find_col(lp, nomcol.c_str());
		CompteurIA = CompteurIA + 1;
		ia[CompteurIA] = CompteurContrainte;
		ja[CompteurIA] = position;
		ar[CompteurIA] = -1.0;

		for (int j = 1; j <= mp; j++)
		{

			oss.str("");
			oss << "x_" << 2 << "_" << j;
			nomcol = oss.str();

			position = glp_find_col(lp, nomcol.c_str());
			CompteurIA = CompteurIA + 1;
			ia[CompteurIA] = CompteurContrainte;
			ja[CompteurIA] = position;
			ar[CompteurIA] = C[4];

		}

	}
	oss.str("");
	oss << "Z";
	nomcol = oss.str();
	glp_add_cols(lp, 1);
	CompteurCol = CompteurCol + 1;
	glp_set_col_name(lp, CompteurCol, nomcol.c_str());
	glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);

	

	// ajout colonne
	//std::ostringstream oss;


	// creation contrainte partie 1. z = ............
	for (int i = 1; i <= 1; i++)
	{
		glp_add_rows(lp, 1);
		CompteurContrainte++;

		std::ostringstream oss;
		oss << "C_" << CompteurContrainte;
		nomcontrainte = oss.str();

		glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
		glp_set_row_bnds(lp, CompteurContrainte, GLP_FX, 0.0, 0.0);


		oss.str("");
		oss << "Z";
		nomcol = oss.str();

		position = glp_find_col(lp, nomcol.c_str());
		CompteurIA = CompteurIA + 1;
		ia[CompteurIA] = CompteurContrainte;
		ja[CompteurIA] = position;
		ar[CompteurIA] = 1.0;



		for (int j = 1; j <= 5; j++)
		{

			oss.str("");
			oss << "z" << j;
			nomcol = oss.str();

			position = glp_find_col(lp, nomcol.c_str());
			CompteurIA = CompteurIA + 1;
			ia[CompteurIA] = CompteurContrainte;
			ja[CompteurIA] = position;
			ar[CompteurIA] = -1.0;

		}
	}

	// les coefficients de la fonction objectif
	oss.str("");
	oss << "Z";
	nomcol = oss.str();
	position = glp_find_col(lp, nomcol.c_str());
	glp_set_obj_coef(lp, position, 1);


	glp_load_matrix(lp, CompteurIA, ia, ja, ar);

	glp_write_lp(lp, NULL, "Aliment.lp");

	glp_simplex(lp, NULL);
	glp_intopt(lp, NULL);


	// valeur de la fonction objectif
	z = glp_mip_obj_val(lp);
	// z = glp_get_obj_val(lp);
	cout << "Z = " << z << endl;

	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= mp; j++)
		{
			oss.str("");
			oss << "x_" << i << "_" << j;
			nomcol = oss.str();

			position = glp_find_col(lp, nomcol.c_str());

			// double res = glp_get_col_prim(lp, position);

			double res = glp_mip_col_val(lp, position);

			cout << nomcol << " = " << res << endl;

		}
		oss.str("");
		oss << "q_" << i;
		nomcol = oss.str();

		position = glp_find_col(lp, nomcol.c_str());

		int res = glp_get_col_prim(lp, position);


		cout << nomcol << " = " << res << endl;

	}


	getchar();

	// liberation m�moire
	glp_delete_prob(lp);
	glp_free_env();



}