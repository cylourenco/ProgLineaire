﻿#include <stdio.h>
#include <stdlib.h>
#include <sstream> 
#include <iostream>


using namespace std;

#include "glpk.h"
#include "Header.h"

// ouverture de fichiers 
/**
 * @brief Lit les données d'une instance depuis un fichier.
 *
 * @param P Structure de données du problème.
 * @param nom Nom du fichier d'instance.
 */
void lire_fichier_instance(t_probleme& P, string nom) {
	// Variables locales pour stocker les valeurs lues
	int dist = 0,
		qtt = 0,
		ind = 0,
		i, j, k;

	// Ouverture du fichier en mode lecture
	std::ifstream fichier_ouvert(nom.c_str());

	if (fichier_ouvert.is_open()) {
		// Lecture des informations relatives au probl�me
		fichier_ouvert >> P.nb_de_client; // Lecture du nombre de clients
		
		// Lecture des distances entre les clients et stockage dans la matrice distance
		for (i = 1; i < P.nb_de_client + 1; i++) {
			for (j = 1; j < P.nb_de_client + 1; j++) {
				fichier_ouvert >> dist; // Lecture de la distance entre deux clients
				P.distance[i][j] = dist; // Stockage de la distance dans la matrice distance
			}
		}

		fichier_ouvert.close(); // Fermeture du fichier apr�s lecture
	}
	else {
		std::cout << "Ouverture ratee" << std::endl; // Affichage d'un message d'erreur si l'ouverture du fichier �choue
	}
}


void afficherTableauArc(int** tab_arc, int nbVilles) {
	cout << "Tableau d'arcs :" << endl;
	for (int i = 1; i <= nbVilles; i++) {
		for (int j = 1; j <= nbVilles; j++) {
			cout << tab_arc[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}



// Fonctions pour la contrainte 4 : detection de cycles
void dfs(int** tab_arc, int numVilles, int ville, std::vector<bool>& visite, std::vector<int>& sousTour) {
	visite[ville] = true;
	sousTour.push_back(ville);

	for (int suivante = 1; suivante <= numVilles; suivante++) {
		if (tab_arc[ville][suivante] == 1 && !visite[suivante]) {
			dfs(tab_arc, numVilles, suivante, visite, sousTour);
		}
	}
}

void detecterSousTours(int** tab_arc, int numVilles, std::vector<std::vector<int>>& sousTours) {
	std::vector<bool> visite(numVilles + 1, false);

	for (int ville = 1; ville <= numVilles; ville++) {
		for (int j = 1; j <= numVilles; j++) {
			if (tab_arc[ville][j] == 1 &&  !visite[ville]) {
				std::vector<int> sousTour;
				dfs(tab_arc, numVilles, ville, visite, sousTour);

				if (sousTour.size() > 0)
					sousTours.push_back(sousTour);
			}
		}
	}
}




void main(void)
{
	t_probleme P;
	lire_fichier_instance(P, "txt/paris.txt");
	vector<vector<int>> sousTours;

	double z;

	bool solutionTrouvee = false;

	while (!solutionTrouvee) {

		// creation des tableaux pour le solveur
		glp_prob *lp;
		int ia[1 + 10000], ja[1 + 10000];
		double ar[1 + 10000];

		// definition du probleme
		lp = glp_create_prob();
		glp_set_prob_name(lp, "TSP");
		glp_set_obj_dir(lp, GLP_MIN);
	

		glp_iocp parm;
		glp_init_iocp(&parm);
		parm.presolve = GLP_ON;  // Utilisation de GLP_ON pour activer la résolution MIP



		// variable globales pour la generation
		int CompteurContrainte = 0;
		int CompteurCol = 0;
		int CompteurIA = 0;
	


		string nomcol;
		string nomcontrainte;
		int position;

		// definition des variables
		for (int i = 1; i <= P.nb_de_client; i++)
		{
			for (int j = 1; j <= P.nb_de_client; j++) {
				std::ostringstream oss;
				oss << "x_" << i << '_' << j;
				nomcol = oss.str();
				glp_add_cols(lp, 1);
				CompteurCol = CompteurCol + 1;
				glp_set_col_name(lp, CompteurCol, nomcol.c_str());
				glp_set_col_kind(lp, CompteurCol, GLP_IV);
				glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);
			}

		}


		std::ostringstream oss;
		oss << "cout";
		nomcol = oss.str();
		glp_add_cols(lp, 1);
		CompteurCol = CompteurCol + 1;
		glp_set_col_name(lp, CompteurCol, nomcol.c_str());
		//glp_set_col_kind(lp, CompteurCol, GLP_IV);
		glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);
	


		// creation Index pour les recherches
		glp_create_index(lp);

		// containte 1. // respecter les délais min entre operations successives de la meme piece
		// St i j-1 + P i j-1 <= Stij
		// -----------
		for (int i = 1; i <= P.nb_de_client; i++)
		{
			glp_add_rows(lp, 1);
			CompteurContrainte++;

			std::ostringstream oss;
			oss << "C_" << CompteurContrainte;
			nomcontrainte = oss.str();

			glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
			glp_set_row_bnds(lp, CompteurContrainte, GLP_FX, 1.0, 1.0);



			for (int j = 1; j <= P.nb_de_client; j++)
			{

				oss.str("");
				oss << "x_" << i << "_" << j;
				nomcol = oss.str();

				position = glp_find_col(lp, nomcol.c_str());
				CompteurIA = CompteurIA + 1;
				ia[CompteurIA] = CompteurContrainte;
				ja[CompteurIA] = position;
				ar[CompteurIA] = 1.0; // coef x
			}
		}

		// contrainte 2
		for (int j = 1; j <= P.nb_de_client; j++)
		{
			glp_add_rows(lp, 1);
			CompteurContrainte++;

			std::ostringstream oss;
			oss << "C_" << CompteurContrainte;
			nomcontrainte = oss.str();

			glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
			glp_set_row_bnds(lp, CompteurContrainte, GLP_FX, 1.0, 1.0);



			for (int i = 1; i <= P.nb_de_client; i++)
			{

				oss.str("");
				oss << "x_" << i << "_" << j;
				nomcol = oss.str();

				position = glp_find_col(lp, nomcol.c_str());
				CompteurIA = CompteurIA + 1;
				ia[CompteurIA] = CompteurContrainte;
				ja[CompteurIA] = position;
				ar[CompteurIA] = 1.0; // coef x
			}
		}

		// contraite 3 : aucun arc entre i et i
		for (int i = 1; i <= P.nb_de_client; i++)
		{
			for (int j = 1; j <= P.nb_de_client; j++)
			{

				if (i == j) {

					glp_add_rows(lp, 1);
					CompteurContrainte++;

					std::ostringstream oss;
					oss << "C_" << CompteurContrainte;
					nomcontrainte = oss.str();

					glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
					glp_set_row_bnds(lp, CompteurContrainte, GLP_FX, 0, 0);

					oss.str("");
					oss << "x_" << i << "_" << j;
					nomcol = oss.str();

					position = glp_find_col(lp, nomcol.c_str());
					CompteurIA = CompteurIA + 1;
					ia[CompteurIA] = CompteurContrainte;
					ja[CompteurIA] = position;
					ar[CompteurIA] = 1.0; // coef x
				}
			}
		}

		// contrainte 4 : cout
		
		glp_add_rows(lp, 1);
		CompteurContrainte++;

		oss.str("");
		oss << "C_" << CompteurContrainte;
		nomcontrainte = oss.str();

		glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
		glp_set_row_bnds(lp, CompteurContrainte, GLP_FX, 0.0, 0.0);

		oss.str("");
		oss << "cout";
		nomcol = oss.str();
		position = glp_find_col(lp, nomcol.c_str());
		CompteurIA = CompteurIA + 1;
		ia[CompteurIA] = CompteurContrainte;
		ja[CompteurIA] = position;
		ar[CompteurIA] = -1.0;

		for (int i = 1; i <= P.nb_de_client; i++)
		{
			for (int j = 1; j <= P.nb_de_client; j++)
			{
				oss.str("");
				oss << "x_" << i << "_" << j;
				nomcol = oss.str();

				position = glp_find_col(lp, nomcol.c_str());
				CompteurIA = CompteurIA + 1;
				ia[CompteurIA] = CompteurContrainte;
				ja[CompteurIA] = position;
				ar[CompteurIA] = P.distance[i][j];
			}
		}
	
		// contrainte 5 : algo sur les cycles

		// tableau de 1 pour representer les arcs
		// Création du tableau d'arcs
		// Allocation dynamique du tableau d'arcs
		int** tab_arc = new int* [P.nb_de_client + 1];
		for (int i = 1; i <= P.nb_de_client; i++) {
			tab_arc[i] = new int[P.nb_de_client + 1];
			// Initialisation à 0
			for (int j = 1; j <= P.nb_de_client; j++) {
				tab_arc[i][j] = 0;
			}
		}

		for (int i = 0; i < sousTours.size(); i++) {

			// definition de la contrainte 4
			glp_add_rows(lp, 1);
			CompteurContrainte++;

			std::ostringstream oss;
			oss << "C_" << CompteurContrainte;
			nomcontrainte = oss.str();

			glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
			glp_set_row_bnds(lp, CompteurContrainte, GLP_UP, sousTours[i].size() - 1, sousTours[i].size() - 1); // -1 car >=

			for (int j = 0; j < sousTours[i].size() - 1; j++)
			{

				oss.str("");
				oss << "x_" << sousTours[i][j] << "_" << sousTours[i][j+1];
				nomcol = oss.str();

				position = glp_find_col(lp, nomcol.c_str());
				CompteurIA = CompteurIA + 1;
				ia[CompteurIA] = CompteurContrainte;
				ja[CompteurIA] = position;
				ar[CompteurIA] = 1.0; // coef x
			}
			oss.str("");
			oss << "x_" << sousTours[i][sousTours[i].size() - 1] << "_" << sousTours[i][0];
			nomcol = oss.str();

			position = glp_find_col(lp, nomcol.c_str());
			CompteurIA = CompteurIA + 1;
			ia[CompteurIA] = CompteurContrainte;
			ja[CompteurIA] = position;
			ar[CompteurIA] = 1.0; // coef x

		}


		// fonction objectif
		oss.str("");
		oss << "cout";
		nomcol = oss.str();
		position = glp_find_col(lp, nomcol.c_str());
		glp_set_obj_coef(lp, position, 1);


		glp_load_matrix(lp, CompteurIA, ia, ja, ar);

		glp_write_lp(lp, NULL, "tsp.lp");


		glp_simplex(lp, NULL);
		glp_intopt(lp, NULL);

		// valeur de la fonction objectif
		z = glp_mip_obj_val(lp);
		// z = glp_get_obj_val(lp);
		cout << "cout = " << z << endl;


		for (int i = 1; i <= P.nb_de_client; i++)
		{
			for (int j = 1; j <= P.nb_de_client; j++)
			{
				oss.str("");
				oss << "x_" << i << "_" << j;
				nomcol = oss.str();
				position = glp_find_col(lp, nomcol.c_str());

				int res = static_cast<int>(glp_mip_col_val(lp, position));

				tab_arc[i][j] = res;

			}

		}

		afficherTableauArc(tab_arc, P.nb_de_client);

		// liberation mémoire
		glp_delete_prob(lp);
		glp_free_env();


		int temp_size = sousTours.size();
		detecterSousTours(tab_arc, P.nb_de_client, sousTours);
		if (sousTours.size() == temp_size + 1) {
			// Une seule tournée est formée -> fin
			solutionTrouvee = true;
		}
		
	}

	cout << "Fin de resolution du TSP avec le cout : " << z << endl;
	
	std::getchar();

}