#pragma once

#ifndef HEADER_H
#define HEADER_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <random>
#include <chrono>

using namespace std;

// D�finition de la taille maximale du nombre de clients et de la valeur "infini"
const int nb_max_client = 99; // Limite maximale du nombre de clients (120 pour pdd)
const int infini = 99999; // Valeur repr�sentant l'infini
const int nb_max = 50; // Nombre maximal de v�hicules
const int it_max = 100; // Nombre maximal d'it�rations
const int taille_liste = 6; // Taille maximale de la liste



// Structure de donn�es repr�sentant le probl�me de routage de v�hicules
typedef struct t_probleme {
    int nb_de_client; // Nombre total de clients
    int distance[nb_max_client + 1][nb_max_client + 1]; // Matrice des distances entre les clients
} t_probleme;

void lire_fichier_instance(t_probleme& P, string nom);
void afficherTableauArc(int** tab_arc, int nbVilles);
void detecterSousTours(int** tab_arc, int numVilles, vector<vector<int>>& sousTours);
void dfs(int** tab_arc, int numVilles, int ville, vector<bool>& visite, vector<int>& sousTour);

#endif
