#include <stdio.h>
#include <stdlib.h>
#include <sstream> 
#include <iostream>


using namespace std;

#include "glpk.h"
#include "Header.h"

// ouverture de fichiers 
/**
 * @brief Lit les donn�es d'une instance depuis un fichier.
 *
 * @param P Structure de donn�es du probl�me.
 * @param nom Nom du fichier d'instance.
 */
void lire_fichier_instance(t_probleme& P, string nom)
{
	int machine = 0,
		duree = 0;
	std::ifstream fichier_ouvert(nom.c_str());

	if (fichier_ouvert.is_open())
	{
		fichier_ouvert >> P.nb_jobs;
		fichier_ouvert >> P.nb_machines;

		for (int i = 1; i < P.nb_jobs + 1; i++)
		{
			for (int j = 1; j < P.nb_machines + 1; j++)
			{
				fichier_ouvert >> machine;
				fichier_ouvert >> duree;
				P.machines[i][j] = machine;
				P.duree_operations[i][j] = duree;
			}
		}
		fichier_ouvert.close();
	}
	else
	{
		std::cout << "Ouverture ratee" << std::endl;
	}
}


void main(void)
{
	t_probleme P;
	lire_fichier_instance(P, "txt/la01.txt");


	// creation des tableaux pour le solveur
	glp_prob *lp;
	int ia[1 + 10000], ja[1 + 10000];
	double ar[1 + 10000], z, x1, x2;

	// definition du probleme
	lp = glp_create_prob();
	glp_set_prob_name(lp, "Job_Shop");
	glp_set_obj_dir(lp, GLP_MIN);


	// variable globales pour la generation
	int CompteurContrainte = 0;
	int CompteurCol = 0;
	int CompteurIA = 0;
	


	string nomcol;
	string nomcontrainte;
	int position;

	// definition des variables - Slides 45
	for (int i = 1; i <= P.nb_jobs; i++)
	{
		for (int j = 1; j <= P.nb_machines; j++) {
			std::ostringstream oss;
			oss << "s_" << i << '_' << j;
			nomcol = oss.str();
			glp_add_cols(lp, 1);
			CompteurCol = CompteurCol + 1;
			glp_set_col_name(lp, CompteurCol, nomcol.c_str());
			glp_set_col_kind(lp, CompteurCol, GLP_IV);
			glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);
		}

	}


	std::ostringstream oss;
	oss << "cout";
	nomcol = oss.str();
	glp_add_cols(lp, 1);
	CompteurCol = CompteurCol + 1;
	glp_set_col_name(lp, CompteurCol, nomcol.c_str());
	//glp_set_col_kind(lp, CompteurCol, GLP_IV);
	glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);
	



	// on ne genere que bijkp
	for (int i = 1; i < P.nb_jobs; i++)
	{
		for (int j = 1; j <= P.nb_machines; j++) 
		{
			for (int k = i+1; k <= P.nb_jobs; k++)
			{
				for (int p = 1; p <= P.nb_machines; p++) 
				{
					// on veut la meme machine
					if (P.machines[i][j] == P.machines[k][p]) {
						std::ostringstream oss;
						oss << "b_" << i << '_' << j << '_' << k << '_' << p;
						nomcol = oss.str();
						glp_add_cols(lp, 1);
						CompteurCol = CompteurCol + 1;
						glp_set_col_name(lp, CompteurCol, nomcol.c_str());
						glp_set_col_kind(lp, CompteurCol, GLP_BV); // BV : boolean
					}
				}
			}
		}
	}

	// creation Index pour les recherches
	glp_create_index(lp);

	// containte 1. // respecter les d�lais min entre operations successives de la meme piece
	// St i j-1 + P i j-1 <= Stij
	// -----------
	for (int i = 1; i <= P.nb_jobs; i++)
	{
		for (int j = 2; j <= P.nb_machines; j++)
		{
			glp_add_rows(lp, 1);
			CompteurContrainte++;

			std::ostringstream oss;
			oss << "C_" << CompteurContrainte;
			nomcontrainte = oss.str();

			glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
			glp_set_row_bnds(lp, CompteurContrainte, GLP_UP, -P.duree_operations[i][j-1], -P.duree_operations[i][j - 1]);

			//std::ostringstream oss;
			oss.str("");
			oss << "s_" << i << '_' << j;
			nomcol = oss.str();

			position = glp_find_col(lp, nomcol.c_str());
			CompteurIA = CompteurIA + 1;
			ia[CompteurIA] = CompteurContrainte;
			ja[CompteurIA] = position;
			ar[CompteurIA] = -1.0; // coef x


			oss.str("");
			oss << "s_" << i << '_' << j-1;
			nomcol = oss.str();

			position = glp_find_col(lp, nomcol.c_str());
			CompteurIA = CompteurIA + 1;
			ia[CompteurIA] = CompteurContrainte;
			ja[CompteurIA] = position;
			ar[CompteurIA] = 1.0; // coef x
			
		}
	}
	


	// contrainte 2 : on ne veut pas de boucles
	// b = 1 si il existe un arc entre ij et kp
	// ------------
	for (int i = 1; i < P.nb_jobs; i++)
	{
		for (int j = 1; j <= P.nb_machines; j++)
		{
			for (int k = i+1; k <= P.nb_jobs; k++)
			{
				for (int p = 1; p <= P.nb_machines; p++)
				{
					if (P.machines[i][j] == P.machines[k][p])
					{

						cout << i << " " << j << " " << k << " " << p << endl;

						glp_add_rows(lp, 1);
						CompteurContrainte++;

						std::ostringstream oss;
						oss << "C_" << CompteurContrainte;
						nomcontrainte = oss.str();

						glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
						glp_set_row_bnds(lp, CompteurContrainte, GLP_UP, -P.duree_operations[k][p], -P.duree_operations[k][p]);

						//std::ostringstream oss;
						oss.str("");
						oss << "s_" << i << '_' << j;
						nomcol = oss.str();

						position = glp_find_col(lp, nomcol.c_str());
						CompteurIA = CompteurIA + 1;
						ia[CompteurIA] = CompteurContrainte;
						ja[CompteurIA] = position;
						ar[CompteurIA] = -1.0;


						oss.str("");
						oss << "s_" << k << '_' << p;
						nomcol = oss.str();

						position = glp_find_col(lp, nomcol.c_str());
						CompteurIA = CompteurIA + 1;
						ia[CompteurIA] = CompteurContrainte;
						ja[CompteurIA] = position;
						ar[CompteurIA] = 1.0;

						oss.str("");
						oss << "b_" << i << '_' << j << '_' << k << '_' << p;
						nomcol = oss.str();

						position = glp_find_col(lp, nomcol.c_str());

						CompteurIA = CompteurIA + 1;
						ia[CompteurIA] = CompteurContrainte;
						ja[CompteurIA] = position;
						ar[CompteurIA] = -H;

						// -----------------------------------------------------

						glp_add_rows(lp, 1);
						CompteurContrainte++;

						oss.str("");
						oss << "C_" << CompteurContrainte;
						nomcontrainte = oss.str();

						glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
						glp_set_row_bnds(lp, CompteurContrainte, GLP_UP, -P.duree_operations[i][j] + H, -P.duree_operations[i][j] + H);

						//std::ostringstream oss;
						oss.str("");
						oss << "s_" << k << '_' << p;
						nomcol = oss.str();

						position = glp_find_col(lp, nomcol.c_str());
						CompteurIA = CompteurIA + 1;
						ia[CompteurIA] = CompteurContrainte;
						ja[CompteurIA] = position;
						ar[CompteurIA] = -1.0;


						oss.str("");
						oss << "s_" << i << '_' << j;
						nomcol = oss.str();

						position = glp_find_col(lp, nomcol.c_str());
						CompteurIA = CompteurIA + 1;
						ia[CompteurIA] = CompteurContrainte;
						ja[CompteurIA] = position;
						ar[CompteurIA] = 1.0;

						oss.str("");
						oss << "b_" << i << '_' << j << '_' << k << '_' << p;
						nomcol = oss.str();

						position = glp_find_col(lp, nomcol.c_str());

						CompteurIA = CompteurIA + 1;
						ia[CompteurIA] = CompteurContrainte;
						ja[CompteurIA] = position;
						ar[CompteurIA] = H;
					}
					

				}

			}
			
		}

	}

	// containte 3. // calcul du cout
// cout >= Sti3 + Pi3
// -----------
	for (int i = 1; i <= P.nb_jobs; i++)
	{
		
		glp_add_rows(lp, 1);
		CompteurContrainte++;

		std::ostringstream oss;
		oss << "C_" << CompteurContrainte;
		nomcontrainte = oss.str();

		glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
		glp_set_row_bnds(lp, CompteurContrainte, GLP_LO, P.duree_operations[i][3], P.duree_operations[i][3]);

		//std::ostringstream oss;
		oss.str("");
		oss << "s_" << i << '_' << 3;
		nomcol = oss.str();

		position = glp_find_col(lp, nomcol.c_str());
		CompteurIA = CompteurIA + 1;
		ia[CompteurIA] = CompteurContrainte;
		ja[CompteurIA] = position;
		ar[CompteurIA] = -1.0; // coef x


		oss.str("");
		oss << "cout";
		nomcol = oss.str();

		position = glp_find_col(lp, nomcol.c_str());
		CompteurIA = CompteurIA + 1;
		ia[CompteurIA] = CompteurContrainte;
		ja[CompteurIA] = position;
		ar[CompteurIA] = 1.0; // coef x

	}


	// les coefficients de la fonction objectif
	oss.str("");
	oss << "cout";
	nomcol = oss.str();
	position = glp_find_col(lp, nomcol.c_str());
	glp_set_obj_coef(lp, position, 1);


	glp_load_matrix(lp, CompteurIA, ia, ja, ar);

	glp_write_lp(lp, NULL, "JobShop.lp");


	glp_simplex(lp, NULL);
	glp_intopt(lp, NULL);

	// valeur de la fonction objectif
	z = glp_mip_obj_val(lp);
	// z = glp_get_obj_val(lp);
	cout << "cout = " << z << endl;
	

	



	for (int i = 1; i <= P.nb_jobs; i++)
	{
		for (int j = 1; j <= P.nb_machines; j++)
		{
			oss.str("");
			oss << "s_" << i << "_" << j;
			nomcol = oss.str();

			position = glp_find_col(lp, nomcol.c_str());

			// double res = glp_get_col_prim(lp, position);

			double res = glp_mip_col_val(lp, position);

			cout << nomcol << " = " << res << endl;

		}

	}

	for (int i = 1; i < P.nb_jobs; i++)
	{
		for (int j = 1; j <= P.nb_machines; j++)
		{
			for (int k = i + 1; k <= P.nb_jobs; k++)
			{
				for (int p = 1; p <= P.nb_machines; p++)
				{
					if (P.machines[i][j] == P.machines[k][p])
					{
						oss.str("");
						oss << "b_" << i << "_" << j << "_" << k << "_" << p;
						nomcol = oss.str();

						position = glp_find_col(lp, nomcol.c_str());

						// double res = glp_get_col_prim(lp, position);

						bool res = glp_mip_col_val(lp, position);

						cout << nomcol << " = " << res << endl;
					}

				}

			}

		}

	}


	getchar();

	// liberation m�moire
	glp_delete_prob(lp);
	glp_free_env();



}