#pragma once

#ifndef HEADER_H
#define HEADER_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <random>
#include <chrono>

using namespace std;

/**
 * @brief Constante pour le nombre maximal de jobs.
 */
const int nb_max_job = 30;


const int H = 9999;

/**
 * @brief Constante pour le nombre maximal de machines.
 */
const int nb_max_machine = 30;

typedef struct t_probleme {
    int nb_jobs;                          ///< Nombre de jobs dans le probl�me.
    int nb_machines;                     ///< Nombre de machines dans le probl�me.
    int duree_operations[nb_max_job][nb_max_machine]; ///< Tableau pour stocker la dur�e des op�rations.
    int machines[nb_max_job][nb_max_machine];        ///< Tableau pour stocker les machines.
} t_probleme;

void lire_fichier_instance(t_probleme& P, string nom);

#endif
