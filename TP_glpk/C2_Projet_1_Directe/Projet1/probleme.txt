\* Problem: short *\

Minimize
 obj: + 4 x1 + 2 x2 + x3

Subject To
 C1: + 2 x2 + x1 >= 0
 C2: + x3 + x2 >= 0
 C3: + x3 + x1 >= 0

End
