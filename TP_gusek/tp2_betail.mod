#Variables
var q{1..2} >= 0;
var x{1..2,1..3} >= 0;
var z;
var z1;
var z2;
var z3;
var z4;
var z5;

#Constantes
param Quantite{1..3};
param D{1..2};
param A{1..3};
param C{1..4};
param P{1..3, 1..3};
param BN{1..3};


#Contraintes

# Quantit� de produit
subject to C1_{i in 1..2}:
    sum {j in 1..3} x[i, j] - q[i] = 0;
subject to C1_1{i in 1..2}:
	q[i] >= D[i];

#Quantit� minimale
subject to C2_{i in 1..2, k in 1..2}:
    sum {j in 1..3} P[j, k] * x[i, j] - q[i] * BN[k] >= 0;

#Quantit� maximale
subject to C3_{i in 1..2, k in 3..3}:
    sum {j in 1..3} P[j, k] * x[i, j] - q[i] * BN[k] <= 0;

#Quantit�
subject to C4_{j in 1..3}:
    sum {i in 1..2} x[i, j] <= Quantite[j];

#Fonctions objectives
subject to C6_:
    sum {i in 1..2, j in 1..3} A[j]*x[i, j] - z1 = 0;
subject to C7_{Op in 1..1}:
    C[Op] * sum {i in 1..2, j in 1..2} x[i, j] - z2 = 0;
subject to C8_{Op in 2..2}:
    C[Op] * sum {i in 1..2, j in 1..3} x[i, j] - z3 = 0;
subject to C9_{Op in 3..3}:
    C[Op] * sum {j in 1..3} x[1, j] - z4 = 0;
subject to C10_{Op in 4..4}:
    C[Op] * sum {j in 1..3} x[2, j] - z5 = 0;
subject to C11_:
    z-z1-z2-z3-z4-z5 = 0;

#Minimiser
minimize f:z;

solve;

#Affichage r�sultat
printf '\n';
printf 'Cout total';
printf '\n';
display z;
printf '\n';
printf 'Composition';
printf '\n';
display x;

data;
    param BN := 1 0.095
                2 0.02
                3 0.06;
    param Quantite :=  1 11900
                2 23500
                3 750;
    param A :=  1 0.8
                2 1.0
                3 0.75;
    param D :=  1 9000
                2 12000;
    param C :=  1 1.50
                2 0.30
                3 2.50
                4 1.0;
    param P :=  [1,*] 1 0.136 2 0.071 3 0.07
                [2,*] 1 0.041 2 0.024 3 0.037
                [3,*] 1  0.05 2 0.003 3 0.25;
end;
