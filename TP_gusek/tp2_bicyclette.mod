#Variables
var s{1..12} >= 0;
var x{1..12};
var y{1..12};
var z;
var z1;
var z2;
var z3;

#Constantes
param S0;
param D{1..12};
param C1;
param C2;
param C3;
param Capa;


#Contraintes

# Stock au mois i doit etre positif
subject to C1_{i in 1..12}:
	x[i] <= Capa;

# Satisfaction demande avec le stock
subject to C2_1{i in 1..1}:
	s[1] = S0 + x[1] + y[1] - D[1];

subject to C2_{i in 2..12}:
    s[i] = s[i - 1] + x[i] + y[i] - D[i];


#Fonctions objectives
subject to C3_:
    C1 * sum {i in 1..12} s[i] - z1 = 0;
subject to C4_:
    C1 * sum {i in 1..12} x[i] - z2 = 0;
subject to C5_:
    C3 * sum {i in 1..12} y[i] - z3 = 0;
subject to C6_:
    z-z1-z2-z3 = 0;

#Minimiser
minimize f:z;

solve;

#Affichage r�sultat
printf '\n';
printf 'Cout total';
printf '\n';
display z;
printf '\n';
printf 'Composition';
printf '\n';
display x;

data;
    param S0 := 2;
	param D := 1 30
				2 15
				3 15 
				4 25
				5 33
				6 40
				7 45
				8 45
				9 26
				10 14
				11 25
				12 30;
	param C1 := 20;
	param C2 := 130;
	param C3 := 160;
	param Capa := 30;
end;